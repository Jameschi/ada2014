#include <stdio.h>
#include <cstdlib>
//construct a structure about box
struct BOX
{
        long long int length;
        long long int width;
		long long int repeat;
}box[100000];
BOX box1[100000];
BOX box2[100000];
int compareW(const void *a, const void *b)
{
	BOX * i = (BOX *) a;
	BOX * j = (BOX *) b;
	if(i -> width < j -> width)
	return -1;
	else if(i -> width == j -> width)
	return i -> length >= j -> length;
	else return 1;
}
int compareL(const void *a, const void *b)
{
	BOX * i = (BOX *) a;
	BOX * j = (BOX *) b;
	if(i -> length < j -> length)
	return -1;
	else if(i -> length == j -> length)
	return i -> width >= j -> width;
	else return 1;
}

//Use n to store the number of elements
int n;
long long int findCrossSubArray(int high1, int high2, BOX array1[], BOX array2[])
{
	long long int count = 0;
	long long int total = 0;
	for(int j = 0; j <= high1; j++)
	count = count + array1[j].repeat + 1;
	int i = high2;
	while(i >= 0)
	{
		if(array2[i].width >= array1[high1].width)
		{
			total += (array2[i].repeat + 1) * count;
			i--;
		}
		else 
		{
			count -= (array1[high1].repeat + 1);
			high1--;
			if(high1 < 0)
			break;
		}
	}
	return total;
}
long long int findSubArray(int low1, int high1, int low2, int high2, BOX array2[])
{
        if(low2 == high2)
		return 0;
        else
        {
				BOX temp1[(high2 - low2) / 2 + 1];
				BOX temp2[(high2 - low2) / 2 + 1];
				int j = 0, k = 0;
				for(int i = low2; i <= high2; i++)
				{
					if(array2[i].length < box1[(high1 + low1) / 2].length)
					{
						temp1[j] = array2[i];
						j++;
					}
					else if(array2[i].length == box1[(high1 + low1) / 2].length && array2[i].width <= box1[(high1 + low1) / 2].width)
					{
						temp1[j] = array2[i];
						j++;
					}	
					else
					{
						temp2[k] = array2[i];
						k++;
					}
				}
				j--;
				k--;
                return findCrossSubArray(j, k, temp1, temp2) + findSubArray(low1, (high1 + low1) / 2, 0, j, temp1) + findSubArray((high1 + low1) / 2 + 1, high1, 0, k, temp2);
		}
}

int main()
{
        int T;
        scanf("%d",&T);
        while(T--)
        {
                scanf("%d",&n);
                int a,b;
                //read in data
                for(int i = 0; i < n; i++)
                {
                        scanf("%d %d",&a,&b);
                        if(a >= b)
                        {
                                box[i].length = a;
                                box[i].width = b;
                        }
                        else
                        {
                                box[i].length = b;
                                box[i].width = a;
                        }
						box[i].repeat = 0;
                }
                qsort(box, n, sizeof(BOX), compareL);
				int temp = n;
				for(int i = 0; i < temp; i++)
				{
					if(box[i].length == box[i+1].length && box[i].width == box[i+1].width)
					{
						box[i].width = box[i].length = -1;
						box[i+1].repeat = box[i].repeat + 1;
						n--;
					}
				}
				int j = 0;
				for(int i = 0; i < temp; i++)
				{
					if(box[i].width != -1)
					{
						box1[j] = box[i];
						j++;
					}
				}
				for(int i = 0; i < n; i++)
				box2[i] = box1[i];
				qsort(box2, n, sizeof(BOX), compareW);
				long long int append = 0;
				for(int i = 0 ; i < n; i++)
				append += (box2[i].repeat * (box2[i].repeat + 1));
				printf("%lld\n", append + findSubArray(0, n - 1, 0, n - 1, box2));
        }
        return 0;
}