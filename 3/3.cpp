#include <cstdio>
#include <iostream>
using namespace std;
int n, m, q;
struct EVENT
{int l, r, v;} event[100000];
struct BLOCK
{int owner;} block[100000];
long long int blockRecord[100000];
struct PLAYER
{
	int goal, success;
	long long int current;
} player[100000];

int binarySearchl(int winorloseBlock[], int low, int high, int eventNO)
{
	if(high == low)
		return low;
	if(event[eventNO].l < winorloseBlock[low])
	{
		return low;
	}
	int mid = (low + high) / 2;
	if(event[eventNO].l <= winorloseBlock[mid] && event[eventNO].l >= winorloseBlock[low])
		return binarySearchl(winorloseBlock, low, mid, eventNO);
	else
		return binarySearchl(winorloseBlock, mid + 1, high, eventNO);
}
int binarySearchr(int winorloseBlock[], int low, int high, int eventNO)
{
	if(high == low)
	{
		if(winorloseBlock[low] == event[eventNO].r)
		return (low + 1);
		else return low;
	}
	if(event[eventNO].r < winorloseBlock[low])
	{
		return low;
	}
	int mid = (low + high) / 2;
	if(event[eventNO].r <= winorloseBlock[mid] && event[eventNO].r >= winorloseBlock[low])
		return binarySearchr(winorloseBlock, low, mid, eventNO);
	else
		return binarySearchr(winorloseBlock, mid + 1, high, eventNO);
}
void findFinish(int playerArray[], int playerNumber, int eventLow, int eventHigh, int winorloseBlock[], int blockNumber)
{
	for(int i = 0; i <= blockNumber; i++)
	blockRecord[winorloseBlock[i]] = 0;
	//Base case
	int searchl = binarySearchl(winorloseBlock, 0, blockNumber, eventLow);
	int searchr = binarySearchr(winorloseBlock, 0, blockNumber, eventLow);
	if(eventLow == eventHigh)
	{
		long long int probe = 0;
		if(event[eventLow].l < winorloseBlock[0])
		{
			if(event[eventLow].r >= winorloseBlock[0])
			blockRecord[winorloseBlock[0]] += event[eventLow].v;
		}
		else if(event[eventLow].l >= winorloseBlock[0] && event[eventLow].l <= winorloseBlock[blockNumber])
		blockRecord[winorloseBlock[searchl]] += event[eventLow].v;
		
		if(event[eventLow].r >= winorloseBlock[0] && event[eventLow].r <= winorloseBlock[blockNumber])
		{
			if(searchr <= blockNumber)
			{
				blockRecord[winorloseBlock[searchr]] += -event[eventLow].v;
			}
		}
		for(int i = 0; i <= blockNumber; i++)
		{
			probe += blockRecord[winorloseBlock[i]];
			player[block[winorloseBlock[i]].owner].current += probe;
		}
		for(int i = 0; i <= playerNumber; i++)
		{
			if(player[playerArray[i]].current >= player[playerArray[i]].goal)
			{
				player[playerArray[i]].success = eventLow;
			}
		}
		return ;
	}
	
	int winPlayer[playerNumber];
	int losePlayer[playerNumber];
	int winBlock[blockNumber];
	int loseBlock[blockNumber];
	for(int i = eventLow; i <= (eventLow + eventHigh) / 2; i++)
	{
		if(event[i].l < winorloseBlock[0])
		{
			if(event[i].r >= winorloseBlock[0])
			blockRecord[winorloseBlock[0]] += event[i].v;
		}
		else if(event[i].l >= winorloseBlock[0] && event[i].l <= winorloseBlock[blockNumber])
		blockRecord[winorloseBlock[binarySearchl(winorloseBlock, 0, blockNumber, i)]] += event[i].v;
		
		if(event[i].r >= winorloseBlock[0] && event[i].r <= winorloseBlock[blockNumber])
		{
			if(binarySearchr(winorloseBlock, 0, blockNumber, i) <= blockNumber)
			blockRecord[winorloseBlock[binarySearchr(winorloseBlock, 0, blockNumber, i)]] += -event[i].v;
		}
	}
	long long int probe = 0;
	for(int i = 0; i <= blockNumber; i++)
	{
		probe += blockRecord[winorloseBlock[i]];
		player[block[winorloseBlock[i]].owner].current += probe;
	}
	int k = 0, j = 0, a = 0, b = 0;
	for(int i = 0; i <= playerNumber; i++)
	{
		if(player[playerArray[i]].current >= player[playerArray[i]].goal)
		{
			winPlayer[k] = playerArray[i];
			k++;
		}
		else
		{	
			losePlayer[j] = playerArray[i];
			j++;
		}
	}	
	k--;
	j--;
	for(int i = 0; i <= blockNumber; i++)
	{
		if(player[block[winorloseBlock[i]].owner].current >= player[block[winorloseBlock[i]].owner].goal)
		{
			winBlock[a] = winorloseBlock[i];
			a++;
		}
		else
		{
			loseBlock[b] = winorloseBlock[i];
			b++;
		}
	}
	a--;
	b--;
	for(int i = 0; i <= j; i++)
	{
		player[losePlayer[i]].goal -= player[losePlayer[i]].current;
	}
	for(int i = 0; i <= playerNumber; i++)
	player[playerArray[i]].current = 0;
	//Recursive Call
	if(k != -1 && a != -1)
	findFinish(winPlayer, k, eventLow, (eventLow + eventHigh) / 2, winBlock, a);
	if(j != -1 && b != -1)
	findFinish(losePlayer, j, (eventLow + eventHigh) / 2 + 1, eventHigh, loseBlock, b);
	return;
}
int main()
{
	int T;
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d %d %d", &n, &m, &q);
		for(int i = 0; i < n; i++)
			scanf("%d", &player[i].goal);
		for(int i = 0; i < m; i++)
		{
			scanf("%d", &block[i].owner);
			block[i].owner--;
		}
		for(int i = 0; i < q; i++)
		{
			scanf("%d %d %d", &event[i].l, &event[i].r, &event[i].v);
			event[i].l--;
			event[i].r--;
		}
		for(int i = 0; i < n; i++)
		player[i].success = -1;
		int playerArray[n];
		for(int i = 0; i < n; i++)
		playerArray[i] = i;
		int temp[m];
		for(int i = 0; i < m; i++)
		temp[i] = i;
		findFinish(playerArray, n - 1, 0, q - 1, temp, m - 1);
		
		for(int i = 0; i < n - 1; i++)
		{
			if(player[i].success != -1)
			printf("%d ", player[i].success + 1);
			else
			printf("%d ",-1);
		}
		if(player[n - 1].success != -1)
			printf("%d", player[n - 1].success + 1);
		else
			printf("%d",-1);
		printf("\n");
		for(int i = 0; i < 100000; i++)
		{
			event[i].l = event[i].r = event[i].v = -1;
			block[i].owner = -1;
			player[i].goal = player[i].current = 0;
			blockRecord[i] = 0;
		}
	}
	return 0;
}