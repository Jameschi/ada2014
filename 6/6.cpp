#include <cstdio>
#include <algorithm>
#include <iostream>
using namespace std;
long long int position[100000];
int n, k;
long long int binaryRange(long long int low, long long int high)
{
	if(low == high)
		return low;
	long long int mid = (high + low) / 2;
	int count = 0, BREAK = 0, i = 0, j = 0;
	while(1)
	{
		while(position[i] - position[j] <= mid)
		{
			i++;
			if(i > n - 1)
			{
				count++;
				BREAK = 1;
				break;
			}
		}
		if(BREAK == 1)
			break;
		j = --i;
		count++;
		while(position[i] - position[j] <= mid)
		{
			i++;
			if(i > n - 1)
			{
				BREAK = 1;
				break;
			}
		}
		if(BREAK == 1)
			break;
		j = i;
	}
	if(count > k)
		return binaryRange(mid + 1, high);
	else	
		return binaryRange(low, mid);
	
}
int main()
{
	int T;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%d %d", &n, &k);
		for(int i = 0; i < n; i++)
			scanf("%d", &position[i]);
		stable_sort(position, position + n);
		cout<<binaryRange(0, position[n - 1] - position[0])<<endl;
	}
	return 0;
}