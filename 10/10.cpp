#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<stack>
using namespace std;

int round[200000];
int answer[200000];
stack<int> mystack;
int main()
{
	int T, kuo;
	scanf("%d", &T);
	while(T--)
	{
		cin >> kuo;
		for(int i = 0; i < kuo; i++)
			cin >> round[i];
		for(int i = 0; i < kuo; i++)
			round[i + kuo] = round[i];
		while(!mystack.empty())
			mystack.pop();
		for(int i = 0; i < (2 * kuo); i++)
		{
			while((!mystack.empty()) && (round[mystack.top()] <= round[i]))
				mystack.pop();
			if(mystack.empty())
			{
				answer[i] = 0;
				mystack.push(i);
			}
			else
			{
				if(mystack.top() >= kuo)
					answer[i] = mystack.top() - kuo + 1;
				else
					answer[i] = mystack.top() + 1;
				mystack.push(i);
			}
		}
		for(int i = kuo; i < (2 * kuo) - 1; i++)
			cout << answer[i] << " ";
		cout << answer[2 * kuo - 1] << endl;
	}
}