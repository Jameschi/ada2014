#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <queue>
#include <iterator>
using namespace std;
int colorvertex[100000];
int record;
typedef struct point{
	int weight, vertex;
}point;
vector<point> graph[100000];
int findBinary(int n)
{
	int count = 0;
	while(n != 0)
	{
		n /= 2;
		count++;
	}
	return count;
}
int findConnected(vector<point> graph[], int node, int edge, int bit)
{
	int count = 0;
	queue<int> myqueue;
	vector<point>::iterator it;
	//check if this is connected using BFS
	myqueue.push(0);
	colorvertex[0] = 1;
	while(!myqueue.empty())
	{
		int x = myqueue.front();
		myqueue.pop();
		for(it = graph[x].begin(); it != graph[x].end(); it++)
		{
			if((((*it).weight & (1 << bit)) == 0) && (colorvertex[(*it).vertex] == 0))
			{
				if((record & (*it).weight) == 0)
				{
					myqueue.push((*it).vertex);
					colorvertex[(*it).vertex] = 1;
					count ++;
				}
			}
		}
	}
	for(int i = 0; i < node; i++)
		colorvertex[i] = 0;
	if(count == (node - 1))//can discard this bit
	{
		record += (1 << bit);
		return 0;
	}
	else return 1;
}
int main()
{
	int T, edge, node, u, v, weight, max, indicate;
	cin>>T;
	while(T--)
	{
		max = 0;
		indicate = 0;
		cin>>node>>edge;
		for (int i = 0; i < node; i++)
			graph[i].clear();
		for (int i = 0; i < edge; i++)
		{
			cin>>u>>v>>weight;	
			if(u != v)
				indicate = 1;
			if(weight > max)
				max = weight;
			point temp;
			temp.weight = weight;
			temp.vertex = v - 1;
			graph[u - 1].push_back(temp);
			temp.vertex = u - 1;
			graph[v - 1].push_back(temp);
		}
		if(indicate == 0)
		{
			cout<<0<<endl;
			continue;
		}
		int bit = findBinary(max);
		int answer = 0;
		record = 0;
		for(int i = 0; i < node; i++)
			colorvertex[i] = 0;
		while(bit >= 1)
		{
			if(findConnected(graph, node, edge, bit - 1) == 1)
			{
				answer += (1 << (bit - 1));
			}
			bit--;
		}
		cout<<answer<<endl;
	}
	return 0;	
}