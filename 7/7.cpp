#include <cstdio>
#include <algorithm>
#include <cmath>
#include <iostream>
using namespace std;
struct WALLET
{
	int value;
	int amount;
	int originalAmount;
};
struct WALLET wallet[10];
int main()
{
	wallet[0].value = 1;
	wallet[1].value = 5;
	wallet[2].value = 10;
	wallet[3].value = 20;
	wallet[4].value = 50;
	wallet[5].value = 100;
	wallet[6].value = 200;
	wallet[7].value = 500;
	wallet[8].value = 1000;
	wallet[9].value = 2000;
	int T, p;
	long long int fake100, fake1000, temp;
	long long int totalMoney, diff, totalAmount, originalDiff;
	long long int sol[4];
	scanf("%d", &T);
	while(T--)
	{
		totalMoney = totalAmount = 0;
		scanf("%d", &p);
		for(int i = 0; i < 10; i++)
		{
			scanf("%d", &wallet[i].amount);
			wallet[i].originalAmount = wallet[i].amount;
			totalAmount += wallet[i].amount;
			totalMoney += wallet[i].amount * wallet[i].value;
		}
		if(totalMoney < p)
		{
			cout << -1 << endl;
			continue;
		}
		diff = originalDiff = totalMoney - p;
		sol[0] = sol[1] = sol[2] = sol[3] = 0;

		//has 50 & has 500
		if(wallet[4].originalAmount > 0 && diff >= 50)
		{
			fake100 = (wallet[4].originalAmount - 1) / 2;
			wallet[4].amount = 0;
			wallet[5].amount += fake100;
			diff -= 50;
			sol[0]++;
		}
		else fake100 = 0;
		if(wallet[7].originalAmount > 0 && diff >= 500)
		{
			fake1000 = (wallet[7].originalAmount - 1) / 2;
			wallet[7].amount = 0;
			wallet[8].amount += fake1000;
			diff -= 500;
			sol[0]++;
		}
		else fake1000 = 0;
		for(int i = 9; i >= 0; i--)
		{
			temp = diff / wallet[i].value;
			if(temp && wallet[i].amount > 0)
			{
				if(temp >= wallet[i].amount)
				{
					sol[0] += wallet[i].amount;
					diff -= wallet[i].amount * wallet[i].value;
					wallet[i].amount = 0;
				}
				else
				{
					sol[0] += temp;
					diff -= temp * wallet[i].value;
					wallet[i].amount -= temp;
				}
			}
			if(diff == 0)
				break;
		}
		if(diff != 0)
			sol[0] = -1;
		else
		{
			if(wallet[8].amount < fake1000)
				sol[0] += (fake1000 - wallet[8].amount);
			if(wallet[5].amount < fake100)
				sol[0] += (fake100 - wallet[5].amount);
		}			
		//consider no 500 & has 50
		diff = originalDiff;
		for(int i = 0; i < 10; i++)
			wallet[i].amount = wallet[i].originalAmount;
		fake1000 = wallet[7].originalAmount / 2;
		wallet[7].amount = 0;
		wallet[8].amount += fake1000;
		if(wallet[4].originalAmount > 0 && diff >= 50)
		{
			wallet[4].amount = 0;
			fake100 = (wallet[4].originalAmount - 1) / 2;
			wallet[5].amount += fake100;
			diff -= 50;
			sol[1]++;
		}
		else
			fake100 = 0;
		for(int i = 9; i >= 0; i--)
		{
			temp = diff / wallet[i].value;
			if(temp && wallet[i].amount > 0)
			{
				if(temp >= wallet[i].amount)
				{
					sol[1] += wallet[i].amount;
					diff -= wallet[i].amount * wallet[i].value;
					wallet[i].amount = 0;
				}
				else
				{
					sol[1] += temp;
					diff -= temp * wallet[i].value;
					wallet[i].amount -= temp;
				}
			}
			if(diff == 0)
				break;
		}
		if(diff != 0)
			sol[1] = -1;
		else 
		{
			if(wallet[8].amount < fake1000)
				sol[1] += (fake1000 - wallet[8].amount);
			if(wallet[5].amount < fake100)
				sol[1] += (fake100 - wallet[5].amount);
		}
		//consider no 50 has 500
		diff = originalDiff;
		for(int i = 0; i < 10; i++)
			wallet[i].amount = wallet[i].originalAmount;
		fake100 = wallet[4].originalAmount / 2;
		wallet[4].amount = 0;
		wallet[5].amount += fake100;
		if(wallet[7].originalAmount > 0 && diff >= 500)
		{
			wallet[7].amount = 0;
			fake1000 = (wallet[7].originalAmount - 1) / 2;
			wallet[8].amount += fake1000;
			diff -= 500;
			sol[2]++;
		}
		else
			fake1000 = 0;
		for(int i = 9; i >= 0; i--)
		{
			temp = diff / wallet[i].value;
			if(temp && wallet[i].amount > 0)
			{
				if(temp >= wallet[i].amount)
				{
					sol[2] += wallet[i].amount;
					diff -= wallet[i].amount * wallet[i].value;
					wallet[i].amount = 0;
				}
				else
				{
					sol[2] += temp;
					diff -= temp * wallet[i].value;
					wallet[i].amount -= temp;
				}
			}
			if(diff == 0)
				break;
		}
		if(diff != 0)
			sol[2] = -1;
		else 
		{
			if(wallet[8].amount < fake1000)
				sol[2] += (fake1000 - wallet[8].amount);
			if(wallet[5].amount < fake100)
				sol[2] += (fake100 - wallet[5].amount);
		}
		//consider no 50 & no 500
		diff = originalDiff;
		for(int i = 0; i < 10; i++)
			wallet[i].amount = wallet[i].originalAmount;
		fake1000 = wallet[7].originalAmount / 2;
		wallet[7].amount = 0;
		wallet[8].amount += fake1000;
		fake100 = wallet[4].originalAmount / 2;
		wallet[4].amount = 0;
		wallet[5].amount += fake100;
		for(int i = 9; i >= 0; i--)
		{
			temp = diff / wallet[i].value;
			if(temp && wallet[i].amount > 0)
			{
				if(temp >= wallet[i].amount)
				{
					sol[3] += wallet[i].amount;
					diff -= wallet[i].amount * wallet[i].value;
					wallet[i].amount = 0;
				}
				else
				{
					sol[3] += temp;
					diff -= temp * wallet[i].value;
					wallet[i].amount -= temp;
				}
			}
			if(diff == 0)
				break;
		}
		if(diff != 0)
			sol[3] = -1;
		else 
		{
			if(wallet[8].amount < fake1000)
				sol[3] += (fake1000 - wallet[8].amount);
			if(wallet[5].amount < fake100)
				sol[3] += (fake100 - wallet[5].amount);
		}
		stable_sort(sol, sol + 4);
		int i;
		for(i = 0; i < 4; i++)
		{
			if(sol[i] != -1)
				break;
		}
		if(i == 4)
		cout << -1 << endl;
		else
		cout << totalAmount - sol[i] << endl;
	}
	return 0;
}