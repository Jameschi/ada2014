#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<iostream>
using namespace std;

string str1,str2;
struct data
{
	char letter;
	int length;
	int left, up, upperleft;
};

data chart[2001][2001];

void parse(string str1, string str2)
{
	int a = str1.size();
	int b = str2.size();
	for(int i = 0; i < a; i++)
	{
		for(int j = 0; j < b; j++)
		{
			//check upper left
			if(str1[i] == str2[j])
			{
				chart[i + 1][j + 1].length = chart[i][j].length + 1;
				chart[i + 1][j + 1].upperleft = 1;
				chart[i + 1][j + 1].letter = str1[i];
			}
			//look up and left to copy the max of them
			else
			{
				if(chart[i][j + 1].length > chart[i + 1][j].length)
				{
					chart[i + 1][j + 1].length = chart[i][j + 1].length;
					chart[i + 1][j + 1].up = 1;
					chart[i + 1][j + 1].letter = chart[i][j + 1].letter;
				}
				else if(chart[i][j + 1].length < chart[i + 1][j].length)
				{
					chart[i + 1][j + 1].length = chart[i + 1][j].length;
					chart[i + 1][j + 1].left = 1;
					chart[i + 1][j + 1].letter = chart[i + 1][j].letter;
				}
				else
				{
					chart[i + 1][j + 1].length = chart[i + 1][j].length;
					chart[i + 1][j + 1].up = chart[i + 1][j + 1].left = 1;
					chart[i + 1][j + 1].letter = min(chart[i + 1][j].letter, chart[i][j + 1].letter);
				}
			}
		}
	}
} 

void answer()
{
	int row = str1.size();
	int column = str2.size();
	while(row != 0 && column != 0)
	{
		if(chart[row][column].upperleft == 1)
		{
			cout<<chart[row][column].letter;
			row--;
			column--;
		}
		else if(chart[row][column].left == 1 && chart[row][column].up == 0)
			column--;
		else if(chart[row][column].left == 0 && chart[row][column].up == 1)
			row--;
		else if(chart[row][column].left == 1 && chart[row][column].up == 1)
		{
			if(chart[row - 1][column].letter == chart[row][column - 1].letter)
			{
				if(chart[row - 1][column].letter == str2[column - 1])
					row--;
				else if(chart[row][column - 1].letter == str1[row - 1])
					column--;
				else
				{
					row--;
					column--;
				}
			}
			else if(chart[row - 1][column].letter < chart[row][column - 1].letter)
				row--;
			else
				column--;
		}
	}
}
int main()
{
	for(int i = 0; i < 2001; i++)
	{
		chart[i][0].length = 0;
		chart[0][i].length = 0;
	}
	int T;
	string startstring;
	scanf("%d",&T);
	
	while(T--)
	{
		cin>>str1;
		cin>>str2;
		reverse(str1.begin(), str1.end());
		reverse(str2.begin(), str2.end());
		parse(str1,str2);
		//dynamic trace the answer from chart
		answer();
		cout<<endl;
		for(int i = 0; i < 2001; i++)
		for(int j = 0; j < 2001; j++)
			chart[i][j].length = chart[i][j].up = chart[i][j].left = chart[i][j].upperleft = chart[i][i].letter = 0;
		str1.clear();
		str2.clear();
	}
	return 0;
}