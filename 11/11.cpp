#include <cstdio>
#include <iostream>
#include <cstring>
using namespace std;

long long int adj[100];// adjacency matrix
int maxc;
void BronKerbosch(int c, long long int P, long long int X)
{
	if ((P == 0) && (X == 0))//P and X are both empty 
	{
		if(c > maxc)
			maxc = c;
		return;
	}
	int p = __builtin_ctzll(P | X);
	long long int Q = (P & (~adj[p]));
    
	while (Q)
	{
		int i = __builtin_ctzll(Q);
		BronKerbosch(c + 1, P & adj[i], X & adj[i]);
		Q &= ~(1LL << i);
		P &= ~(1LL << i);
		X |= (1LL << i);
	}
	return;
}
 
int main()
{
    int T;
	cin >> T;
	while(T--)
	{
		memset(adj, 0, sizeof(adj));//adj has 64 bits
		maxc = 0;
		int n, e;
		cin >> n >> e;
		int a, b;
		for(int i = 0; i < e; i++)
		{
			cin >> a >> b;
			adj[a] |= (1LL << b);
			adj[b] |= (1LL << a);
		}
		for(int i = 0; i < n; i++)//transform to clique
		{
			adj[i] = ~adj[i];
			adj[i] &= ((1LL << n) - 1);
			adj[i] &= ~(1LL << i);//remove self-loop
		}
		BronKerbosch(0, (1LL << n) - 1, 0);
		cout << maxc << endl;
	}
}