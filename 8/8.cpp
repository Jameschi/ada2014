#include <cstdio>
#include <iostream>
#include <queue>
#define trap -3
#define Empty -2
#define beverage -1
#define start 0
#define end 1
#define black 2
#define white 3
#define gray 4
using namespace std;

int l;
typedef struct Queue{
    int x, y, z, value, d; 
}Queue;

typedef struct property{
	int color;
	int d;
	int value;
	int type;
}prop;
prop room[100][100][100];

int direction[6][3] = {{-1, 0, 0}, {0, 0, -1}, {0, 1, 0}, {0, 0, 1}, {0, -1, 0}, {1, 0, 0}};

int isNotBound(int i, int j, int k)
{
    return ((i >= 0) && (i < l) && (j >= 0) && (j < l) && (k >= 0) && (k < l));
}
queue<Queue>myqueue;
queue<Queue>tempQueue;
int Traverse(int x, int y, int z, int dx, int dy, int dz, int *value)
{ 
	int i, j, k;
	Queue init;
	init.x = x;
	init.y = y;
	init.z = z;
	init.value = 0;
	init.d = 0;
	myqueue.push(init);
	Queue temp;
	while(1){
	while(!myqueue.empty())
	{  
		temp = myqueue.front();
		tempQueue.push(temp);
		myqueue.pop();
		for(int w = 0; w < 6; w++)
        {
            i = temp.x + direction[w][0];
            j = temp.y + direction[w][1];
            k = temp.z + direction[w][2];
			if(isNotBound(i, j, k) && (room[i][j][k].color == white) && ((room[i][j][k].type == Empty) || (room[i][j][k].type == beverage) || (room[i][j][k].type == end)))
			{
				room[i][j][k].d = room[temp.x][temp.y][temp.z].d + 1;
				if(room[i][j][k].type == beverage)
				{
					if(room[temp.x][temp.y][temp.z].value + 1 > room[i][j][k].value)
						room[i][j][k].value = room[temp.x][temp.y][temp.z].value + 1;
				}
				else
				{
					if(room[temp.x][temp.y][temp.z].value > room[i][j][k].value)
						room[i][j][k].value = room[temp.x][temp.y][temp.z].value;
				}
			}
        }
		room[temp.x][temp.y][temp.z].color = black;
	}
	while(!tempQueue.empty())
	{
		temp = tempQueue.front();
		tempQueue.pop();
		for(int w = 0; w < 6; w++)
        {
			i = temp.x + direction[w][0];
            j = temp.y + direction[w][1];
            k = temp.z + direction[w][2];
			if(isNotBound(i, j, k) && (room[i][j][k].color == white) && ((room[i][j][k].type == Empty) || (room[i][j][k].type == beverage) || (room[i][j][k].type == end)))
			{
				room[i][j][k].color = gray;
				if((i == dx) && (j == dy) && (k == dz))
				{
					*value = room[i][j][k].value;
					return room[i][j][k].d;
				}
				Queue insert;
				insert.x = i;
				insert.y = j;
				insert.z = k;
				insert.value = room[i][j][k].value;
				insert.d = room[i][j][k].d;
				myqueue.push(insert);
			}
		}
	}
	if(myqueue.empty())
		return -1;
	}
}

int main()
{
    int Sx, Sy, Sz, Ex, Ey, Ez;//Sxyz = coordinates of starting point 
    char cell;
	int T;
	int ret;
	int value;
	scanf("%d", &T);
    while(T--)
    {
        scanf("%d", &l);
        for(int i = 0; i < l; i++)
            for(int j = 0; j < l; j++)
                for(int k = 0; k < l; k++)
                {
                    room[i][j][k].color = white;
					room[i][j][k].d = 0;
					room[i][j][k].value = 0;
					cin>>cell;
                    switch(cell)
                    {
                        case '#' : room[i][j][k].type = trap; break;
                        case '.' : room[i][j][k].type = Empty; break;
                        case 'B' : room[i][j][k].type = beverage; break;
                        case 'S' : room[i][j][k].type = start; Sx = i; Sy = j; Sz = k; break;
                        case 'E' : room[i][j][k].type = end; Ex = i; Ey = j; Ez = k; break;
						default : break;
                    }
                }
		value = 0;
		while(!myqueue.empty())
			myqueue.pop();
		while(!tempQueue.empty())
			tempQueue.pop();
        ret = Traverse(Sx, Sy, Sz, Ex, Ey, Ez, &value);
        if(ret != -1)
			printf("%d %d\n", ret, value);
        else printf("Fail OAQ\n");
    }
    return 0;
}