﻿#include<cstdio>
#include<algorithm>
#include<iostream>
using namespace std;
int n, m; 
long long dp[16][16][1<<16];
int main()
{
	int T;
	char temp;
	scanf("%d", &T);
	while(T--)
    {
		scanf("%d %d", &n, &m);
		int chart[n][m];//棋盤
		for(int i = 0; i < n; i++)
		{
			for(int j = 0; j < m; j++)
			{
				cin>>temp;//讀入棋盤符號X.
				if(temp == 'X')
					chart[i][j] = 1;
				else	
					chart[i][j] = 0;
			}
		}
		
		for(int i = 0; i <= n; i++)
		for(int j = 0; j < m; j++)
		for(int k = 0; k < (1 << m); k++)
			dp[i][j][k] = 0;//歸零
		dp[0][0][0] = 1;//初始狀態是1
		for(int i = 0; i < n; i++)
        {
			for(int j = 0; j < m; j++)
			{
				for(int k = 0; k < (1 << m); k++)//跑過所有可能的問號情況
				{
					if(dp[i][j][k] >= (1000000000 + 7))//判斷是否需除以免溢位
					dp[i][j][k] %= (1000000000 + 7);
					if((!(k & (1 << (m - 1)))) && (chart[i][j] == 0))//最高位不是1且棋盤沒打X
					{
						if(j == (m - 1))
							(dp[i + 1][0][k << 1] += dp[i][j][k]);//放1X1且需要換行
						else
							(dp[i][j + 1][k << 1] += dp[i][j][k]);//放1X1不用換行
					}
					if((j < (m - 1)) && (chart[i][j] == 0) && (chart[i][j + 1] == 0) && (!(k & (1 << (m - 1)))) && (!(k & (1 << (m - 2)))))//最高位和第二高位不是1且棋盤在這兩個位置沒打X
					{
						if(j == (m - 2))
							dp[i + 1][0][k << 2] += dp[i][j][k];//右放1X2且需要換行
						else if(j <= (m - 3))
							dp[i][j + 2][k << 2] += dp[i][j][k];//右放1X2不用換行
					}
					if((i < (n - 1)) && (chart[i][j] == 0) && (chart[i + 1][j] == 0) && (!(k & (1 << (m - 1)))))//最高位和最低位不是1且棋盤在這兩個位置沒打X
					{
						if(j == (m - 1))
						{
							dp[i + 1][0][(k << 1) | 1] += dp[i][j][k];//下放2X1且需要換行
						}	
						else
						{
							dp[i][j + 1][(k << 1) | 1] += dp[i][j][k];//下放2X1不用換行
						}	
					}
					if((i < (n - 1)) && (j < (m - 1)) && (chart[i][j] == 0) && (chart[i][j + 1] == 0) && (chart[i + 1][j + 1] == 0) && (chart[i + 1][j] == 0) && (!(k & (1 << (m - 1)))) && (!(k & (1 << (m - 2)))))//最高位和第二高位不是1且棋盤在2X2覆蓋範圍沒打X
					{
						if(j == (m - 2))
							dp[i + 1][0][(k << 2) | 3] += dp[i][j][k];//放2X2且需要換行
						else
							dp[i][j + 2][(k << 2) | 3] += dp[i][j][k];//放2X2不用換行	
					}
					//處理有打X或是本來就是1的情況
					if((j == (m - 1)) && (((k & (1 << (m - 1)))) || (chart[i][j] == 1)))//在column的邊界要換行
					{
						if(k & (1 << (m - 1)))
							dp[i + 1][0][(k << 1) ^ (1 << m)] += dp[i][j][k];//最高位是1往後移一格(要換行)
						else	
							dp[i + 1][0][k << 1] += dp[i][j][k];//最高位不是1且棋盤打X往後移一格(要換行)
					}
					if((j <= (m - 2)) && (((k & (1 << (m - 1)))) || (chart[i][j] == 1)))//不在column邊界不用換行
					{
						if(k & (1 << (m - 1)))
							dp[i][j + 1][(k << 1) ^ (1 << m)] += dp[i][j][k];//最高位是1往後移一格
						else	
							dp[i][j + 1][k << 1] += dp[i][j][k];//最高位不是1且棋盤打X往後移一格
					}
				}
            }
		}	
		long long answer = (dp[n][0][0])%(1000000000 + 7);//dp[n][0][0]是最後答案儲存的地方 代表前面nXm都跑完了
		printf("%lld\n", answer);
   }
   return 0;
}